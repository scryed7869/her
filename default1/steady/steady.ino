/*************************************************** 
  This is an example for our Adafruit 16-channel PWM & Servo driver
  Servo test - this will drive 16 servos, one after the other

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products/815

  These displays use I2C to communicate, 2 pins are required to  
  interface. For Arduino UNOs, thats SCL -> Analog 5, SDA -> Analog 4

  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// called this way, it uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// you can also call it with a different address you want
Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41);

// Depending on your servo make, the pulse width min and max may vary, you 
// want these to be as small/large as possible without hitting the hard stop
// for max range. You'll have to tweak them as necessary to match the servos you
// have!

//min 200
//max 500
//#define SERVOMIN  349 // this is the 'minimum' pulse length count (out of 4096)
//#define SERVOMAX  351 // this is the 'maximum' pulse length count (out of 4096)


#define SERVOMIN  200 // this is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  500 // this is the 'maximum' pulse length count (out of 4096)
#define DELAYSERVO 30
// our servo # counter
uint8_t servonum = 0;
uint16_t delayse= 100;
int incomingByte = 0; 
String a="";

int bottom[6] = {0,3,6,9,12,15};
int middle[6] = {1,4,7,10,13,16};
int top[6] = {2,5,6,11,14,17};

int servos[18]={8,9,10,4,5,6,0,1,2,8,9,10,4,5,6,0,1,2};




//int servos[18]={10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180};
//int servos[18]={200,216,233,250,266,283,300,316,333,350,366,383,400,416,433,450,466,483,500};

void setup() {
  //delay(600000);
  Serial.begin(9600);
  Serial.println("16 channel Servo test!");
  
  pwm.begin();
  
  pwm.setPWMFreq(60);  // Analog servos run at ~60 Hz updates

  
  pwm2.begin();
 
  pwm2.setPWMFreq(60);  // Analog servos run at ~60 Hz updates

Serial.println("wtf!!!");
//moveServos(3,250,450);
moveServos(1,345,350);
//moveServos(2,200,250);
moveServos(3,450,460);
  yield();
}



void loop() {

//walk();

  
// oscilateServos(3,350,200);
//  oscilateServos(2,280,200);
delay(DELAYSERVO); 
pwm.setPWM(0, 0, 350);
pwm.setPWM(1, 0, 350);
pwm.setPWM(2, 0, 350);
pwm.setPWM(3, 0, 350);
pwm.setPWM(4, 0, 350);
pwm.setPWM(5, 0, 350);
pwm.setPWM(6, 0, 350);
pwm.setPWM(7, 0, 350);
pwm.setPWM(8, 0, 350);
pwm2.setPWM(0, 0, 350);
pwm2.setPWM(1, 0, 350);
pwm2.setPWM(2, 0, 350);
pwm2.setPWM(3, 0, 350);
pwm2.setPWM(4, 0, 350);
pwm2.setPWM(5, 0, 350);
pwm2.setPWM(6, 0, 350);
pwm2.setPWM(7, 0, 350);
pwm2.setPWM(8, 0, 350);
  // Drive each servo one at a time

 //if (Serial.available() > 0) {
                // read the incoming byte:
              //  incomingByte = Serial.read();
          //      a = Serial.readString();
             
                // say what you got:
               // Serial.print("I received: ");
                //Serial.println(incomingByte, DEC);
      //  }
}

int walk(){
  int MIN=250;
  int MAX=300;
  Serial.print("\n");
  Serial.print("\n");
Serial.print("Phase 1\n");
//delay(4000);
  
   for (uint16_t pulselen = MIN; pulselen < MAX; pulselen++) {
    delay(DELAYSERVO);
      pwm.setPWM(servos[13], 0, pulselen);
      pwm2.setPWM(servos[1], 0, pulselen);
      pwm2.setPWM(servos[7], 0, pulselen);

      int pulselen2=250-(pulselen-MIN);
       Serial.print(pulselen2);
      pwm.setPWM(servos[14], 0, pulselen2+100);
      pwm2.setPWM(servos[2], 0, pulselen+100);
      pwm2.setPWM(servos[8], 0, pulselen+100);


      pulselen2=350-(pulselen-MIN);
      pwm2.setPWM(servos[5], 0, pulselen2+50);
      pwm.setPWM(servos[11], 0, pulselen+50);
      pwm.setPWM(servos[17], 0, pulselen+50);
      
   }

Serial.print("Phase 2\n");
//delay(4000);

    for (uint16_t pulselen = MAX; pulselen > MIN; pulselen--) {
    delay(DELAYSERVO); 
      pwm.setPWM(servos[13], 0, pulselen);
      pwm2.setPWM(servos[1], 0, pulselen);
      pwm2.setPWM(servos[7], 0, pulselen);

      
   }

   
Serial.print("Phase 3\n");
//delay(4000);
     for (uint16_t pulselen = MAX; pulselen > MIN; pulselen--) {
    delay(DELAYSERVO);

      int pulselen2=200+(MAX-pulselen);
      Serial.print(pulselen2);
      pwm.setPWM(servos[14], 0, (pulselen2+100));
      pwm2.setPWM(servos[2], 0, pulselen+100);
      pwm2.setPWM(servos[8], 0, pulselen+100);


      pulselen2=MIN+(MAX-pulselen);
      pwm2.setPWM(servos[4], 0, pulselen2);
      pwm.setPWM(servos[16], 0, pulselen2);
      pwm.setPWM(servos[10], 0, pulselen2);

      pulselen2=300+(MAX-pulselen);
      pwm2.setPWM(servos[5], 0, pulselen2+50);
      pwm.setPWM(servos[11], 0, pulselen+50);
      pwm.setPWM(servos[17], 0, pulselen+50);

      
      
   }

   Serial.print("Phase 4\n");
//delay(4000);
     for (uint16_t pulselen = MAX; pulselen > MIN; pulselen--) {
    delay(DELAYSERVO);

      pwm2.setPWM(servos[4], 0, pulselen);
      pwm.setPWM(servos[16], 0, pulselen);
      pwm.setPWM(servos[10], 0, pulselen);  
   }

   
   Serial.print("****end****\n");

   
  
}


int oscilateServos(int sectionn,int MAX, int MIN){

  int section[6];
  int i;
  if(sectionn==1){
    for (i = 0; i < 6; i = i + 1) {
        section[i]=top[i];
      }
  }else if(sectionn==2){
    for (i = 0; i < 6; i = i + 1) {
        section[i]=middle[i];
      }
  }else if(sectionn ==3){
    for (i = 0; i < 6; i = i + 1) {
        section[i]=bottom[i];
      }
  }
  

 // for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
  for (uint16_t pulselen = MAX; pulselen > MIN; pulselen--) {
   delay(DELAYSERVO);
    //pwm.setPWM(servonum, 0, pulselen);
   // pwm.setPWM(0, 0, pulselen);
   // pwm.setPWM(1, 0, pulselen);
    //pwm.setPWM(2, 0, pulselen);
    //pwm2.setPWM(servonum, 0, pulselen);
    int j;
    for(j=0;j<6;j++){
      int servo=section[j];
      int servoLogic=servos[servo];
      if(servo<=8)
        pwm.setPWM(servoLogic, 0, pulselen);
      else
        pwm2.setPWM(servoLogic, 0, pulselen);
    }
    
  }

 
  //for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
 for (uint16_t pulselen = MIN; pulselen < MAX; pulselen++) {
    delay(DELAYSERVO);
    //pwm.setPWM(servonum, 0, pulselen);
  //  pwm.setPWM(0, 0, pulselen);
   // pwm.setPWM(1, 0, pulselen);
   // pwm.setPWM(2, 0, pulselen);
    //pwm2.setPWM(servonum, 0, pulselen);
    int j;
    for(j=0;j<6;j++){
      int servo=section[j];
      int servoLogic=servos[servo];
      if(servo<=8)
        pwm.setPWM(servoLogic, 0, pulselen);
      else
        pwm2.setPWM(servoLogic, 0, pulselen);
    }
  }


  
  
}

int moveServos(int sectionn,int MAX, int MIN){

  int section[6];
  int i;
  if(sectionn==1){
    for (i = 0; i < 6; i = i + 1) {
        section[i]=top[i];
      }
  }else if(sectionn==2){
    for (i = 0; i < 6; i = i + 1) {
        section[i]=middle[i];
      }
  }else if(sectionn ==3){
    for (i = 0; i < 6; i = i + 1) {
        section[i]=bottom[i];
      }
  }
  

 // for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {

 if(MAX<MIN){
   for (uint16_t pulselen = MAX; pulselen < MIN; pulselen++) {
   delay(DELAYSERVO);
    int j;
    for(j=0;j<6;j++){
      int servo=section[j];
      int servoLogic=servos[servo];
      if(servo<=8)
        pwm.setPWM(servoLogic, 0, pulselen);
      else
        pwm2.setPWM(servoLogic, 0, pulselen);
    } 
  }
 }else{
   for (uint16_t pulselen = MAX; pulselen > MIN; pulselen--) {
   delay(DELAYSERVO);
    int j;
    for(j=0;j<6;j++){
      int servo=section[j];
      int servoLogic=servos[servo];
      if(servo<=8)
        pwm.setPWM(servoLogic, 0, pulselen);
      else
        pwm2.setPWM(servoLogic, 0, pulselen);
    } 
  }
 }
}


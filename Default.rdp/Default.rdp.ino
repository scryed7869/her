/*************************************************** 
  This is an example for our Adafruit 16-channel PWM & Servo driver
  Servo test - this will drive 16 servos, one after the other

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products/815

  These displays use I2C to communicate, 2 pins are required to  
  interface. For Arduino UNOs, thats SCL -> Analog 5, SDA -> Analog 4

  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// called this way, it uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// you can also call it with a different address you want
Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41);

// Depending on your servo make, the pulse width min and max may vary, you 
// want these to be as small/large as possible without hitting the hard stop
// for max range. You'll have to tweak them as necessary to match the servos you
// have!

//min 200
//max 500
//#define SERVOMIN  349 // this is the 'minimum' pulse length count (out of 4096)
//#define SERVOMAX  351 // this is the 'maximum' pulse length count (out of 4096)


#define SERVOMIN  250 // this is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  351 // this is the 'maximum' pulse length count (out of 4096)

// our servo # counter
uint8_t servonum = 0;
uint16_t delayse= 100;
int incomingByte = 0; 
String a="";

int bottom[6] = {0,3,6,9,12,15};
int middle[6] = {1,4,7,10,13,16};
int top[6] = {2,5,6,11,14,17};


void setup() {
  //delay(600000);
  Serial.begin(9600);
  Serial.println("16 channel Servo test!");
  
  pwm.begin();
  
  pwm.setPWMFreq(60);  // Analog servos run at ~60 Hz updates

  
  pwm2.begin();
  
  pwm2.setPWMFreq(60);  // Analog servos run at ~60 Hz updates


  yield();
}



void loop() {
  // Drive each servo one at a time

 //if (Serial.available() > 0) {
                // read the incoming byte:
              //  incomingByte = Serial.read();
          //      a = Serial.readString();
        //      Serial.println(a);
                // say what you got:
               // Serial.print("I received: ");
                //Serial.println(incomingByte, DEC);
      //  }

  
 // Serial.println(servonum);
if (servonum > 15){
  servonum = 0;
  return;
}
 
if(servonum==3 | servonum==7 | servonum==11 | servonum==11 | servonum==12 | servonum==13 | servonum==14 | servonum==15 ){
  servonum++;
  return;
}
//if(servonum%3==0 & servonum!=0){
 // servonum++;
 // return;
//}

for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
   delay(50);
    pwm.setPWM(servonum, 0, pulselen);
   // pwm.setPWM(0, 0, pulselen);
   // pwm.setPWM(1, 0, pulselen);
    //pwm.setPWM(2, 0, pulselen);
    pwm2.setPWM(servonum, 0, pulselen);
  }

 
  for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
    delay(50);
    pwm.setPWM(servonum, 0, pulselen);
  //  pwm.setPWM(0, 0, pulselen);
   // pwm.setPWM(1, 0, pulselen);
   // pwm.setPWM(2, 0, pulselen);
    pwm2.setPWM(servonum, 0, pulselen);
  }

  delay(0);
  
  //delay(100);

  servonum ++;
  if (servonum > 15) servonum = 0;
}
